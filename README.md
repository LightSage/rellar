# Rellar

A Pollr inspired mod log bot.

![Example](https://i.imgur.com/9HtGqP8.png)

## Self-Hosting Instructions

- Copy `config.json.example` to `config.json` and configure all the parts
  - Bot Managers should be filled with User ID(s) that can manage the bot. Obviously, only give people you trust access.
  - Mod Log Channel should be filled in as an integer. 
    - ❌ `"622107323766276114"` This is wrong.
    - ✅ `622107323766276114` This is correct.
  - **Note:** If you decide to change the channel of where mod logs should go, do note the old channel's mod logs cannot be edited.
  - Special Roles should be filled with a list of role IDs that you want to log.
- Create and add the bot to your guild. More info here -> [https://discordpy.readthedocs.io/en/latest/discord.html](https://discordpy.readthedocs.io/en/latest/discord.html)
  - **Make sure to give the bot permissions to `View Audit Log`**
- Install Python3.6+
- Install the requirements in `requirements.txt`
- Run the bot `python rellar.py`
  
NOTE: You are responsible of making backups of the `data` folder in case of data corruption!

## License
```
# Rellar - A Pollr inspired mod log bot
# https://gitlab.com/LightSage/rellar
#
# Copyright (C) 2019 LightSage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
```