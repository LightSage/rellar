# Rellar - A Pollr inspired mod log bot
# https://gitlab.com/LightSage/rellar
#
# Copyright (C) 2019 LightSage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import os


def escape_message(text: str):
    return str(text).replace("@", "@ ").replace("<#", "# ").replace("<&", "& ")


async def is_bot_manager(ctx):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True
    ext = return_json("config.json")
    bm = ctx.author.id in ext['bot_managers']
    if bm:
        return True
    return False


def return_json(file_path: str):
    """Checks if a file's path exists"""
    if os.path.isfile(file_path):
        with open(file_path, "r") as jsonf:
            return json.load(jsonf)
    else:
        return {}


async def get_user_from_id(bot, id):
    try:
        user = await bot.fetch_user(id)
    except Exception:
        user = id
    return str(user)


async def add_specialrole_entry(bot, role_type: str, role, mod, target,
                                reason):
    """Adds a special role case to the mod log

    Arguments:
    --------------
    type: `str`
        The type of action that was done.
        Actions can be one of the following: Added or Removed
    role:
        The role that was added or removed.
    mod:
        The responsible moderator who did the action
    target:
        The member that got an action taken against them
    reason: `str`
        The reason why an action was taken
    """
    safe_name = str(target)
    ext = return_json("data/modcase.json")
    case_number = return_json("data/case_number.json")
    config = return_json("config.json")
    cn = "case_number"
    if cn not in case_number:
        case_number[cn] = 0
    case_number["case_number"] += 1
    cn = case_number['case_number']
    json.dump(case_number, open("data/case_number.json", "w"))
    reason = escape_message(reason)
    # Send our message
    chan = bot.get_channel(bot.config["modlog_channel"])
    msg = f"**Special Role {role_type}** | Case {cn}\n"\
          f"**User:** {safe_name} ({target.id})\n"\
          f"**Role:** {role.name} ({role.id})\n"
    if reason:
        msg += f"**Reason:** {reason}\n"
    else:
        msg += f"**Reason:** Responsible moderator, please add a reason"\
               f" using `{config['prefixes'][0]}reason {cn} <reason>`\n"
    msg += f"**Responsible moderator:** {str(mod)}"
    retmsg = await chan.send(msg)
    if str(cn) not in ext:
        ext[str(cn)] = {"case_info": {"action": f"Special Role {role_type}",
                                      "moderator": mod.id,
                                      "target": target.id,
                                      "reason": reason,
                                      "message_id": retmsg.id,
                                      "role": {"role_name": role.name,
                                               "role_id": role.id}}}
    json.dump(ext, open("data/modcase.json", "w"))
    return cn


async def modlog_edit_reason(bot, case: int, reason):
    """Edits a mod log case's reason

    Arguments:
    --------------
    case: `int`
        The case number of the case for editing.
    reason: `str`
        The new reason string to add to the case.
    """
    ext = return_json("data/modcase.json")
    cn = str(case)
    if cn not in ext:
        return False
    ext[cn]["case_info"]["reason"] = reason
    channel = bot.get_channel(bot.config["modlog_channel"])
    pmsg = await channel.fetch_message(ext[cn]["case_info"]["message_id"])
    safe_name = await get_user_from_id(bot, ext[cn]["case_info"]["target"])
    mod = await get_user_from_id(bot, ext[cn]["case_info"]["moderator"])
    reason = escape_message(reason)
    if ext[cn]['case_info']['action'] == "Special Role Added":
        role_name = ext[cn]['case_info']['role']['role_name']
        role_id = ext[cn]['case_info']['role']['role_id']
        nsmg = f"**{ext[cn]['case_info']['action']}** | Case {case}\n"\
               f"**User:** {safe_name} ({ext[cn]['case_info']['target']})\n"\
               f"**Role:** {role_name} ({role_id})\n"\
               f"**Reason:** {reason}\n"\
               f"**Responsible moderator:** {mod}"
        await pmsg.edit(content=nsmg)
    elif ext[cn]['case_info']['action'] == "Special Role Removed":
        role_name = ext[cn]['case_info']['role']['role_name']
        role_id = ext[cn]['case_info']['role']['role_id']
        nsmg = f"**{ext[cn]['case_info']['action']}** | Case {case}\n"\
               f"**User:** {safe_name} ({ext[cn]['case_info']['target']})\n"\
               f"**Role:** {role_name} ({role_id})\n"\
               f"**Reason:** {reason}\n"\
               f"**Responsible moderator:** {mod}"
        await pmsg.edit(content=nsmg)
    else:
        nmsg = f"**{ext[cn]['case_info']['action']}** | Case {case}\n"\
               f"**User:** {safe_name} ({ext[cn]['case_info']['target']})\n"\
               f"**Reason:** {reason}\n"\
               f"**Responsible moderator:** {mod}"
        await pmsg.edit(content=nmsg)
    json.dump(ext, open("data/modcase.json", "w"))


async def add_modlog_entry(bot, mod_action: str, mod, target, reason):
    """Adds a case to the mod log

    Arguments:
    --------------
    mod_action: `str`
        The type of action that was done.
        Actions can be one of the following: Ban, Kick, Unban
    mod:
        The responsible moderator who did the action
    target:
        The member that got an action taken against them
    reason: `str`
        The reason why an action was taken
    """
    safe_name = str(target)
    ext = return_json("data/modcase.json")
    case_number = return_json("data/case_number.json")
    config = return_json("config.json")
    cn = "case_number"
    if cn not in case_number:
        case_number[cn] = 0
    case_number["case_number"] += 1
    cn = case_number['case_number']
    json.dump(case_number, open("data/case_number.json", "w"))
    reason = escape_message(reason)
    # Send our message
    chan = bot.get_channel(bot.config["modlog_channel"])
    msg = f"**{mod_action}** | Case {cn}\n"\
          f"**User:** {safe_name} ({target.id})\n"
    if reason:
        msg += f"**Reason:** {reason}\n"
    else:
        msg += f"**Reason:** Responsible moderator, please add a reason"\
               f" using `{config['prefixes'][0]}reason {cn} <reason>`\n"
    msg += f"**Responsible moderator:** {str(mod)}"
    retmsg = await chan.send(msg)
    if str(cn) not in ext:
        ext[str(cn)] = {"case_info": {"action": mod_action,
                                      "moderator": mod.id,
                                      "target": target.id,
                                      "reason": reason,
                                      "message_id": retmsg.id}}
    json.dump(ext, open("data/modcase.json", "w"))
    return cn
