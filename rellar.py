# Rellar - A Pollr inspired mod log bot
# https://gitlab.com/LightSage/rellar
#
# Copyright (C) 2019 LightSage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import discord
import logging
from discord.ext import commands
import sys
import os
import traceback
import logging.handlers
from utils.modcase import is_bot_manager

# Create data folder if not found
if not os.path.exists("data"):
    os.makedirs("data")


log = logging.getLogger('discord')
log.setLevel(logging.INFO)
log_file_name = "data/rellar.log"
handler = logging.FileHandler(filename=log_file_name,
                              mode='w', encoding='utf-8')
log_format = logging.Formatter('[%(asctime)s] {%(filename)s:%(lineno)d} '
                               '%(levelname)s - %(message)s')
handler.setFormatter(log_format)
stdout_handler = logging.StreamHandler(sys.stdout)
log.addHandler(handler)
stdout_handler.setFormatter(log_format)
log.addHandler(stdout_handler)

config = json.load(open("config.json", "r"))


def _callable_prefix(bot, message):
    prefixes = config["prefixes"]
    return commands.when_mentioned_or(*prefixes)(bot, message)


initial_extensions = ["cogs.case", "jishaku"]


bot = commands.Bot(command_prefix=_callable_prefix,
                   description="Rellar, a Pollr "
                               "inspired mod log bot")
bot.log = log
bot.config = config

if __name__ == '__main__':
    for ext in initial_extensions:
        try:
            bot.load_extension(ext)
        except Exception:
            log.error(f'Failed to load cog {ext}.')
            log.error(traceback.print_exc())


@bot.event
async def on_command_error(ctx, error):
    err_msg = f"Error with \"{ctx.message.content}\" from "\
              f"\"{ctx.message.author} ({ctx.message.author.id})"\
              f": {str(error)}"
    log.error(err_msg)
    if isinstance(error, commands.NoPrivateMessage):
        await ctx.send('This command cannot be '
                       'used in private messages.')
    elif isinstance(error, commands.CheckFailure):
        return await ctx.send('You do not have permission'
                              ' to use this command.')
    elif isinstance(error, commands.MissingPermissions):
        roles_needed = '\n- '.join(error.missing_perms)
        return await ctx.send(f"{ctx.author.mention}: You don't have the right"
                              " permissions to run this command. You need: "
                              f"```- {roles_needed}```")
    help_text = f"\n\nPlease see `{ctx.prefix}help "\
                f"{ctx.command}` for more info about this command."
    if isinstance(error, commands.BadArgument):
        content = await commands.clean_content().convert(ctx, str(error))
        return await ctx.send(f"{ctx.author.mention}: You gave incorrect "
                              f"arguments. `{content}` {help_text}")
    elif isinstance(error, commands.MissingRequiredArgument):
        content = await commands.clean_content().convert(ctx, str(error))
        return await ctx.send(f"{ctx.author.mention}: You gave incomplete "
                              f"arguments. `{content}` {help_text}")


@bot.event
async def on_ready():
    log.info(f'\nLogged in as: {bot.user.name} - '
             f'{bot.user.id}\ndpy version: '
             f'{discord.__version__}\n')


@bot.event
async def on_message(message):
    if message.author.bot:
        return
    ctx = await bot.get_context(message)
    await bot.invoke(ctx)


@bot.command()
async def about(ctx):
    """Gives info about this bot"""
    e = discord.Embed(title="Rellar",
                      description="A Pollr inspired mod log bot")
    e.set_thumbnail(url=bot.user.avatar_url)
    e.url = "https://gitlab.com/LightSage/rellar"
    await ctx.send(embed=e)


@bot.group(invoke_without_command=True)
async def settings(ctx):
    """Views the bot's settings."""
    e = discord.Embed(title="Rellar Settings", color=0xFF8C00)
    bm_1 = [f"<@{n}>" for n in bot.config["bot_managers"]]
    bm = ', '.join(bm_1)
    e.add_field(name="Bot Managers", value=bm)
    e.add_field(name="Log Channel", value=f"<#{bot.config['modlog_channel']}>",
                inline=False)
    await ctx.send(embed=e)


@commands.check(is_bot_manager)
@settings.command(name="reload")
async def _reloadsettings(ctx):
    """Reloads the bot's config file.

    Bot Managers only!"""
    ext = json.load(open("config.json", "r"))
    bot.config = ext
    await ctx.send("\N{OK HAND SIGN}")


@commands.check(is_bot_manager)
@bot.command(name="exit")
async def _exit(ctx):
    """Exits the bot.

    Bot Managers only!"""
    await ctx.send("\N{WAVING HAND SIGN}")
    await bot.close()

bot.run(config["token"], bot=True, reconnect=True)
