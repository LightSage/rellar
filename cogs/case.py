# Rellar - A Pollr inspired mod log bot
# https://gitlab.com/LightSage/rellar
#
# Copyright (C) 2019 LightSage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from discord.ext import commands
import asyncio
import discord
from utils.modcase import add_modlog_entry, modlog_edit_reason, return_json
from utils.modcase import is_bot_manager, add_specialrole_entry
import json


class Case(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        try:
            self.latest = return_json("data/case_number.json")["case_number"]
        except KeyError:
            self.latest = 0

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        # Wait for Audit Log to update
        await asyncio.sleep(0.5)
        # Cap off at 25 for safety measures
        async for entry in guild.audit_logs(limit=25,
                                            action=discord.AuditLogAction.ban):
            if entry.target == user:
                author = entry.user
                reason = entry.reason if entry.reason else ""
                self.latest = await add_modlog_entry(self.bot,
                                                     "Ban",
                                                     author,
                                                     user,
                                                     reason)
                break

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        # Wait for Audit Log to update
        await asyncio.sleep(0.5)
        async for entry in member.guild.audit_logs(limit=25):
            # If ban comes up first, break
            if entry.action == discord.AuditLogAction.ban:
                break
            if entry.target == member:
                author = entry.user
                reason = entry.reason if entry.reason else ""
                self.latest = await add_modlog_entry(self.bot,
                                                     "Kick",
                                                     author,
                                                     member,
                                                     reason)
                break

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):
        # Wait for Audit Log to update
        await asyncio.sleep(0.5)
        # Something to make flake8 be quiet
        action = discord.AuditLogAction.unban
        # Cap off at 25 for safety measures
        async for entry in guild.audit_logs(limit=25,
                                            action=action):
            if entry.target == user:
                author = entry.user
                reason = entry.reason if entry.reason else ""
                self.latest = await add_modlog_entry(self.bot,
                                                     "Unban",
                                                     author,
                                                     user,
                                                     reason)
                break

    async def role_check(self, role, after, typeofact: str):
        # If role ID matches, go ahead poll audit logs
        if role.id in self.bot.config["special-roles"]:
            await asyncio.sleep(0.5)
            act = discord.AuditLogAction.member_role_update
            async for entry in after.guild.audit_logs(limit=25,
                                                      action=act):
                if entry.target == after:
                    rolesafter = dict(entry.after)
                    idlist = []
                    for r in rolesafter['roles']:
                        idlist.append(r.id)
                    if role.id in idlist:
                        author = entry.user
                        reason = entry.reason if entry.reason else ""
                        self.latest = await add_specialrole_entry(self.bot,
                                                                  typeofact,
                                                                  role,
                                                                  author,
                                                                  after,
                                                                  reason)
                        break

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        if before.roles != after.roles:
            nroles = [role for role in after.roles if role not in before.roles]
            eroles = [role for role in before.roles if role not in after.roles]
            if nroles != 0:
                for role in nroles:
                    await self.role_check(role, after, "Added")
            if eroles != 0:
                for role in eroles:
                    await self.role_check(role, after, "Removed")

    @commands.has_permissions(kick_members=True)
    @commands.command()
    async def reason(self, ctx, case_number, *, reason: str):
        """Adds or edits a reason to a case.

        You need Kick Members permission to edit
        a case reason"""
        if case_number.lower() == "latest":
            if self.latest != 0:
                case_number = self.latest
            else:
                return await ctx.send("There are no cases.")
        case = await modlog_edit_reason(self.bot, case_number, reason)
        if case is False:
            return await ctx.send("Case does not exist!")
        await ctx.send("\N{OK HAND SIGN}")

    @commands.check(is_bot_manager)
    @commands.command()
    async def reset(self, ctx):
        """Removes all cases from the json file.

        Bot Managers only!"""
        cases = return_json("data/modcase.json")
        cases = {}
        json.dump(cases, open("data/modcase.json", "w"))
        casenumber = return_json("data/case_number.json")
        casenumber = {}
        json.dump(casenumber, open("data/case_number.json", "w"))
        await ctx.send("\N{OK HAND SIGN}")


def setup(bot):
    bot.add_cog(Case(bot))
